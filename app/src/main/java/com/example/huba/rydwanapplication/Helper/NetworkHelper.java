package com.example.huba.rydwanapplication.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Huba on 12.09.2017.
 */

public class NetworkHelper {

    private Context context;

    public NetworkHelper(Context context)
    {
        this.context = context;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean result = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return result;
    }
}
