package com.example.huba.rydwanapplication.CallClasses;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Huba on 11.09.2017.
 */
@Setter
@Getter
@NoArgsConstructor
public class AdTask implements Serializable {
    private String fromPlace;
    private String toPlace;
    private String date;
    private Integer seatsLeft;
    private String comment;
    private Integer price;

}
