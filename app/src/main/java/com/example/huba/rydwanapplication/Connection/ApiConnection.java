package com.example.huba.rydwanapplication.Connection;

import android.content.Context;
import android.widget.Toast;

import com.example.huba.rydwanapplication.CallClasses.TestTask;
import com.example.huba.rydwanapplication.InterfacesApi.TaskService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Huba on 12.09.2017.
 */

public class ApiConnection {

    private Context context;

    private Retrofit retrofit;

    public ApiConnection(Context context)
    {
        this.context=context;
        //TODO domyslnie będzie tutaj builder z linkiem na naszym serwerze
        this.retrofit = new Retrofit
            .Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    }

    public void testConnection()
    {
        TaskService taskService = retrofit.create(TaskService.class);

        Call<TestTask> call = taskService.getTasks();

        //Asynchroniczne pobieranie danych z serwera
        call.enqueue(new Callback<TestTask>() {
            @Override
            public void onResponse(Call<TestTask> call, Response<TestTask> response) {
                Toast.makeText(context, response.body().getBody().toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<TestTask> call, Throwable t) {

            }
        });
    }
}
