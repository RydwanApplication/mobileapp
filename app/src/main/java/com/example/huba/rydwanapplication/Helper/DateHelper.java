package com.example.huba.rydwanapplication.Helper;

import java.util.Calendar;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Huba on 09.08.2017.
 */

@Setter
@Getter
public class DateHelper {
    private int minute;
    private int hour;
    private int day;
    private int month;
    private int year;
    Calendar date;


    public DateHelper() {
        date = Calendar.getInstance();
        this.day = date.get(Calendar.DAY_OF_MONTH);
        this.month = date.get(Calendar.MONTH);
        this.year = date.get(Calendar.YEAR);
    }

    public DateHelper(int day, int month, int year) {
        date = Calendar.getInstance();
        date.set(year,month,day);
        this.day = date.get(Calendar.DAY_OF_MONTH);
        this.month = date.get(Calendar.MONTH);
        this.year = date.get(Calendar.YEAR);
    }

    @Override
    public String toString() {
        return day+"/"+month+"/"+year;
    }
}
