package com.example.huba.rydwanapplication.InterfacesApi;

import com.example.huba.rydwanapplication.CallClasses.TestTask;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Huba on 11.09.2017.
 */

public interface TaskService {
    @GET("posts/1")
    Call<TestTask> getTasks();
}
