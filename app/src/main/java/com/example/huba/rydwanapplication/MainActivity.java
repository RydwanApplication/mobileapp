package com.example.huba.rydwanapplication;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.example.huba.rydwanapplication.Connection.ApiConnection;
import com.example.huba.rydwanapplication.Helper.DateHelper;
import com.example.huba.rydwanapplication.Helper.NetworkHelper;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @BindView (R.id.editText1)
    EditText fromDestination;
    @BindView(R.id.editText2)
    EditText toDestination;
    @BindView(R.id.button)
    Button searchButton;
    @BindView(R.id.dateButton)
    ImageButton dateButton;
    @BindView(R.id.textView)
    TextView showDate;

    private DatePickerDialog datePickerDialog;
    private DateHelper dateHelper;
    private NetworkHelper networkHelper;
    private ApiConnection apiConnection;
    private MainActivity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        dateHelper = new DateHelper();
        showDate.setText(dateHelper.toString());

        apiConnection = new ApiConnection(activity);
        networkHelper = new NetworkHelper(activity);

        buttonCalendarListener();
    }

    private void buttonCalendarListener() {
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentYear = dateHelper.getYear();
                int currentMonth = dateHelper.getMonth();
                int currentDay = dateHelper.getDay();
                // date picker dialog
                datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                               DateHelper chosedDate = new DateHelper(dayOfMonth,monthOfYear,year);
                               showDate.setText(chosedDate.toString());
                            }
                        }, currentYear, currentMonth, currentDay);
                datePickerDialog.show();
            }
        });
    }

    public void searchMethod(View view) throws IOException {

        if(networkHelper.isConnected())
        {
            apiConnection.testConnection();
        }
        else
        {
            Toast.makeText(activity, "You are offline", Toast.LENGTH_SHORT).show();
        }

    }

    public void currentData(View view) {
    }

    public void addRide(View view) {
    }
}
