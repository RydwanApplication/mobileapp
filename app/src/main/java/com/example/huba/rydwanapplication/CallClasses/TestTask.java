package com.example.huba.rydwanapplication.CallClasses;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Huba on 11.09.2017.
 */

@Setter
@Getter
@NoArgsConstructor
public class TestTask implements Serializable {
    private Integer userId;
    private Integer id;
    private String title;
    private String body;


}
